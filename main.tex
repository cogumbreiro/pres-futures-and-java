\documentclass{beamer}
% hide navigation menu
\setbeamertemplate{navigation symbols}{}
% show frame number
\setbeamertemplate{footline}[frame number]
% using code listings
\usepackage{listings}

\setbeamertemplate{frametitle}
{%
\begin{flushleft}%
    \LARGE{\insertframetitle}%
\end{flushleft}%
}

\title{Futures and Java}
\date{February 17$^\text{th}$, 2014}

\institute{Imperial College London}

\newcommand{\bigtitle}[1]{%
  \Huge%
  \begin{center}#1\end{center}%
}
\newenvironment{imgframe}[1]{
\usebackgroundtemplate{%
  \includegraphics[width=\paperwidth,height=\paperheight]{#1}}%
\begin{frame}[plain]%
}{%
\end{frame}%
\usebackgroundtemplate{}%
}

\newcommand{\eg}{\textit{e.g.},\@\xspace}
\newcommand{\ie}{\textit{i.e.,}\@\xspace}
\newcommand{\etal}{\textit{et al.}\@\xspace}
\newcommand{\cf}{\textit{cf.}\@\xspace}

\author{Tiago Cogumbreiro}


% \AtBeginSection[] % Do nothing for \se
% {
%   \begin{frame}<beamer>
%     \frametitle{Outline}
%     \tableofcontents[currentsection]
%   \end{frame}
% }

\begin{document}

\begin{frame} 
\titlepage
\end{frame}

\begin{frame}
  \begin{itemize}
  \item \textit{Safe Futures for Java}. Adam Welc, Suresh Jagannathan
    and Antony Hosking. (OOPSLA '05)
  \item \textit{Deadlocks and Livelocks in Concurrent Objects with
      Futures}. Elena Giachino, Cosimo Laneve, and Tudor
    Lascu. (Unpublished '12) \end{itemize}
  \vfill
  {\tiny{Copyright notice: this presentation uses content from
      the papers referred above.}}
\end{frame}

\begin{frame}
  \bigtitle{Safe Futures for Java}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programming Fibonacci}
  \begin{lstlisting}[language=Java]
int fib(int n)
{
    if (n < 2)
        return n;
    int x = fib(n-1);
    int y = fib(n-2);
    return x + y;
}
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programming Fibonacci with futures\\{\small{in pseudocode}}}
  \begin{lstlisting}[language=Java,emph={future,Future,get},  emphstyle={\color{blue}}]
int fib(int n)
{
    if (n < 2)
        return n;
    Future<int> x = future fib(n-1);
    int y = fib(n-2);
    return x.get() + y;
}
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programming with futures\\{\small{in JSE7}}}
  \begin{lstlisting}[language=Java,emph={fork,join},  emphstyle={\color{blue}}]
class Fibonacci extends RecursiveTask<Integer> {
   final int n;
   Fibonacci(int n) { this.n = n; }
   Integer compute() {
     if (n < 2)
        return n;
     Fibonacci x = new Fibonacci(n - 1);
     x.fork();
     Fibonacci y = new Fibonacci(n - 2);
     return x.join() + y;
   }
}  
  \end{lstlisting}

\end{frame}

\begin{frame}
  \begin{center}
    \frametitle{Syntax}
  \includegraphics[width=0.8\textwidth]{oopsla05-syntax}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \frametitle{Syntax of a task}
  \includegraphics{oopsla05-oper-sem-future-notes}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Reduction rule for \texttt{future}}
  \begin{center}
  \includegraphics[width=\textwidth]{oopsla05-oper-sem-future}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Reduction rule for \texttt{get}}
  \begin{center}
  \includegraphics[width=0.7\textwidth]{oopsla05-oper-sem-get}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Reduction rule for \texttt{write}}
  \begin{center}
  \includegraphics[width=0.7\textwidth]{oopsla05-oper-sem-write}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Definitions}
  \begin{center}
  \includegraphics[width=\textwidth]{oopsla05-defs}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Safe futures}
  \begin{center}
  \includegraphics[width=\textwidth]{oopsla05-safety}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Forward safety violation}
  \begin{center}
  \includegraphics[height=0.5\textheight]{oopsla05-impl-forward}
  \end{center}
  \begin{itemize}
  \item Solution: restart the future
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Backward safety violation}
  \begin{center}
  \includegraphics[height=0.5\textheight]{oopsla05-impl-backward}
  \end{center}
  \begin{itemize}
  \item Solution: versioning
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusion}
  \begin{itemize}
  \item assumption: futures should preserve serial semantics
  \item solution: futures seen as (optimistic) lightweight transactions
  \end{itemize}
\end{frame}

\begin{frame}
  \bigtitle{Deadlocks and Livelocks in Concurrent Objects with
    Futures}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Featherweight Java with futures: \texttt{FJf}
  \item Type inference 
  \item Sound (but not complete) deadlock and livelock analysis
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Does Java program deadlock?}
  \begin{lstlisting}[language=Java,emph={fork,join},  emphstyle={\color{blue}}]
class Fact extends RecursiveTask<Integer> {
   final int n;
   Fact(int n) { this.n = n; }
   Integer compute() {
     if (n == 0)
        return 1;
     Fact x = new Fact(n - 1);
     x.fork();
     return n * x.join();
   }
}  
  \end{lstlisting}
\end{frame}

\begin{frame}
  \bigtitle{No, but in \texttt{FJf} it does.}
\end{frame}

\begin{frame}
  \frametitle{Cooperative multithreading}
  \begin{itemize}
  \item \emph{One} processor, multiple threads
  \item Explicit scheduling (usually called \emph{yield})
  \item No preemption
  \item Concurrent execution, not parallel
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Syntax}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-syntax}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Task term}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-oper-sem-syntax}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Method invocation}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-oper-sem-invoke}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Get future value}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-oper-sem-get}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Yield the processor}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-oper-sem-await}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{\texttt{FJf}'s type system: a primer}
  \begin{itemize}
  \item tracks resource dependencies;
  \item $a$ invokes \lstinline{e.get} on $b$ generates $(a,b)$
  \item Finite State Machine: at different stages, we have different
    dependencies;
  \item types are called \textbf{contracts} (inferred)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Deadlock example}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-deadlock}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Deadlock example (fix)}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-deadlock-fix}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Deadlock analysis technique}
  \begin{center}
  \includegraphics[width=\textwidth]{fjf-deadlock-infer}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Deadlock analysis technique: overview}
  Run up-to a fixed approximant~$n$:
  \begin{itemize}
  \item If the $n$-th approximant is not a fixpoint and consumes the object names,
  \item then the $(n + 1)$-th approximant will reuse the same object
    names used by the $n$-th approximant, and
  \item similarly for the $(n + 2)$-th approximant till a fixpoint is reached.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
  \item use abstract descriptions of method invocations (contracts)
  \item analyse these relations with a FSM
  \item sound and incomplete (static analysis)
  \end{itemize}
\end{frame}

\begin{frame}
  \bigtitle{Questions?}
  \vfill
  \begin{center}
  \footnotesize{\href{https://bitbucket.org/cogumbreiro/pres-futures-and-java}{(This presentation can be forked.)}}
  \end{center}
\end{frame}

\end{document}
